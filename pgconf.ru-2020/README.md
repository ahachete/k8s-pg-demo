# Containers

Get the container image:
```
podman pull postgres
```

Launch Postgres container:
```
podman run --name pg -d --rm postgres
```

Access container:
```
podman exec -it -u postgres pg psql
```

Finish container:
```
podman stop pg
```


# Kubernetes

## 01: Running a basic Postgres pod 

Deploy a pod:
```
01-pod.yaml
```

Connect to postgres container:
```
kubectl exec -it pod-pg -c pg -- psql ## ?? psql or sh ?
```

Describe pod, show logs
```
kubectl describe pod pod-pg
kubectl logs pod-pg -c pg
```

## 02: Making data persistent

### With hostPath.

```
02a-pod.yaml	
```

Restart container, verify data is persisted.


## Dynamic persistent storage via StorageClass and PersistentVolumeClaims

Crete PersistentVolumeClaim
```
02b-pvc.yaml
```

Crete Pod
```
02b-pod.yaml
```

## 03: Polishing the pod

### Add namespace

```
3a
```

### Container environment variables

```
3b
```

### ConfigMap

```
3c
```

### Secret

```
3d
```

### Labels

```
3e
```

### Readiness probe

```
3f
```

## 04: Deployment
```
04
```

Scale the replicas. Watch the PVCs. What happened with the volume(s) ?


## 05: StatefulSet

Create service
```
05a
```

Create sts
```
05b
```

Check connection directly to pod IP, to service IP, and delete pod and repeat
```
kubectl run -it --image docker.io/library/postgres:12.1 -- bash
```

Connect via DNS:
```
pg1.service-pg
```
