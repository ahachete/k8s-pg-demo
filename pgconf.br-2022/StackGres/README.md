## Install monitoring dependencies (optional)


Prometheus, Grafana and AlertManager stack:
```
kubectl create namespace monitoring
helm install --namespace monitoring prometheus-operator --version 36.0.0 prometheus-community/kube-prometheus-stack
```

Install StackGres, using a LoadBalancer if on a cloud environment (remove otherwise):

```
kubectl create namespace stackgres

helm install --namespace stackgres stackgres-operator \
    --set grafana.autoEmbed=true \
    --set-string grafana.webHost=prometheus-operator-grafana.monitoring \
    --set-string grafana.secretNamespace=monitoring \
    --set-string grafana.secretName=prometheus-operator-grafana \
    --set-string grafana.secretUserKey=admin-user \
    --set-string grafana.secretPasswordKey=admin-password \
    --set-string adminui.service.type=LoadBalancer \
stackgres-charts/stackgres-operator
```
