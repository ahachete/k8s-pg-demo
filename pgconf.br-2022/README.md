# Containers

Launch Postgres container:
```
podman run -d --rm --name pg -e POSTGRES_PASSWORD=a postgres 
```

Access container:
```
podman exec -it -u postgres pg psql
```

Finish container:
```
podman stop pg
```

Mount volume for persistence:
```
mkdir /tmp/pgdata
podman run -d --rm --name pg -e POSTGRES_PASSWORD=a -v /tmp/pgdata:/var/lib/postgresql/data postgres
```

# Kubernetes the hard way

## 01: Running a basic Postgres pod 

Deploy a pod:
```
kubectl apply -f 01-pod.yaml
```

Connect to postgres container:
```
kubectl exec -it pod-pg -c pg -- sh
# su - postgres
# psql
```

Describe pod, show logs
```
kubectl describe pod pod-pg
kubectl logs pod-pg -c pg
```

## 02: Making data persistent

### With hostPath.

```
kubectl apply -f 02a-pod.yaml	
```

Restart container, verify data is persisted.


## Dynamic persistent storage via StorageClass and PersistentVolumeClaims

Crete PersistentVolumeClaim
```
kubectl apply -f 02b1-pvc.yaml
```

Create busybox Pod to fix volume:
```
kubectl apply -f 02b2-pod.yaml
kubectl exec -it busybox -- sh
# rmdir /var/lib/postgresql/data/lost+found
kubectl delete pod busybox
```

Create Postgres pod:
```
kubectl apply -f 02b3-pod.yaml
```


# StackGres

Follow [StackGres Tutorial](https://stackgres.io/doc/latest/tutorial/)
